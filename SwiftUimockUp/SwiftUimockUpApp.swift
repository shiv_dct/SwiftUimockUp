//
//  SwiftUimockUpApp.swift
//  SwiftUimockUp
//
//  Created by Danish computer technologies on 05/04/22.
//

import SwiftUI

// The @main attribute identifies the app’s entry point.
@main

struct SwiftUimockUpApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
