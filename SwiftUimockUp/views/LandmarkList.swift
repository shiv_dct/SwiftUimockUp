//
//  LandmarkList.swift
//  SwiftUimockUp
//
//  Created by Danish computer technologies on 05/04/22.
//

import SwiftUI

struct LandmarkList: View {
    var body: some View {
//        List {
//                   LandmarkRow(landmark: landmarks[1])
//                   LandmarkRow(landmark: landmarks[2])
//               }
        NavigationView {
        List(landmarks, id: \.id){ landmark in
            NavigationLink {
                              // LandmarkDetail()
                LandmarkDetail(landmark: landmark)
                           } label: {
                               LandmarkRow(landmark: landmark)
                           }
            
           // LandmarkRow(landmark: landmark)
            
        }
        .navigationTitle("Landmarks")
        }
    }
}

struct LandmarkList_Previews: PreviewProvider {
    static var previews: some View {
//        LandmarkList()
//            .previewDevice(PreviewDevice(rawValue: "iPhone SE (2nd generation)"))
        
        ForEach(["iPhone SE (2nd generation)", "iPhone XS Max"], id: \.self) { deviceName in
                    LandmarkList()
                        .previewDevice(PreviewDevice(rawValue: deviceName))
                        .previewDisplayName(deviceName)
                }
    }
}
