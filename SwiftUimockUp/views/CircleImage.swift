//
//  CircleImage.swift
//  SwiftUimockUp
//
//  Created by Danish computer technologies on 05/04/22.
//

import SwiftUI

struct CircleImage: View {
    var image: Image
    
    var body: some View {
//        Image("PAN_Shivendra")
//            .clipShape(Circle())
//            .overlay {
//                           Circle().stroke(.white, lineWidth: 4)
//                       }
//            .shadow(radius: 7)
        image
                   .clipShape(Circle())
                   .overlay {
                       Circle().stroke(.white, lineWidth: 4)
                   }
                   .shadow(radius: 7)
    }
        
}

struct CircleImage_Previews: PreviewProvider {
    static var previews: some View {
        //CircleImage()
        CircleImage(image: Image("turtlerock"))
    }
}
